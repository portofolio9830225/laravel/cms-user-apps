<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User Apps</title>

    <!-- Fonts and icons -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon/favicon-16x16.png') }}">

    <link href="{{ asset('css/material-dashboard-pro.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/chosen.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />

    <style>
        .card [class*="card-header-"]:not(.card-header-icon):not(.card-header-text):not(.card-header-image) {
            border-radius: 6px;
            margin-top: -50px;
            padding: 10px;
        }

        .card .card-header-warning:not(.card-header-icon):not(.card-header-text) {
            box-shadow: 0 4px 20px 0 rgba(0, 0, 0, .14), 0 7px 10px -5px rgba(128, 0, 0, .4);
        }

        .card .card-header-warning:not(.card-header-icon):not(.card-header-text) {
            /*background: linear-gradient(60deg, #64b5f6, #0d47a1);*/
            background: linear-gradient(60deg, #64b5f6, #ffffff);
        }

        .card [class*="card-header-"] {
            margin: 0 20px;
        }

        .form-control::placeholder {
            font-size: 12px;
            line-height: 1.8 !important;
        }

        .input-group-text i {
            font-size: 18px;
        }

        div.input-group-text {
            width: 50px;
            text-align: center;
        }

        .label_top {
            font-size: 35px;
            color: white;
            margin-bottom: 15px;
            margin-top: 5px;
            font-family: 'Ubuntu', sans-serif;
        }

        .input-group {
            padding-right: 20px;
        }
    </style>

</head>

<body class="off-canvas-sidebar">

    <div class="wrapper wrapper-full-page">
        <div class="page-header  header-filter" filter-color="black"
            style="background-image: url('{{ asset('img/login.jpg') }}'); background-size: cover; background-position: top center;">

            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
                        <form class="form" method="POST" action="{{ route('index.post') }}" autocomplete="off"
                            id="login">
                            @csrf
                            <div class="card card-login card-hidden">

                                <div class="card-body ">
                                    <span class="bmd-form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fa fa-user-circle-o"></i>
                                                </div>
                                            </div>

                                            <input type="text" class="form-control" placeholder="No HP..."
                                                name="username" value="" autofocus required>
                                        </div>
                                    </span>

                                    <span class="bmd-form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fa fa-envelope-o"></i>
                                                </div>
                                            </div>

                                            <input type="email" class="form-control" placeholder="Email..."
                                                name="email" value="" autofocus required>
                                        </div>
                                    </span>
                                </div>
                                <div class="card-footer justify-content-center">
                                    <button type="submit" class="btn btn-warning btn-md"
                                        style="width:180px; padding: 8px 30px;">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>

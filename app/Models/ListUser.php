<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListUser extends Model
{
    use HasFactory;
    protected $table = 'list_user';
    protected $guarded = [];
    
    public $timestamps = false;
}
